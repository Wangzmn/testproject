plugins {
  kotlin("multiplatform")
  id("maven-publish")
  `java-library`
  signing
}

group = "cn.aimdie"
version = "1.2"

kotlin {
  /**
   * 方法：fun jvm(name: String = "jvm")
   * 1、他就是目录：jvmMain。
   * 2、将jvmMain目录
   */
  jvm {
    compilations.all {
      kotlinOptions.jvmTarget = "1.8"
    }
    testRuns["test"].executionTask.configure {
      useJUnit()
    }
  }
  
  /**
   * 给各个模块添加依赖。
   */
  sourceSets {
    named("commonMain") {
      dependencies {
        api("org.jetbrains.kotlin:kotlin-stdlib-common:${Versions.kotlinVersion}")
        api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.0")
      }
    }
    named("commonTest") {
      dependencies {
        implementation(kotlin("test-junit"))
//        api("jetbrain")
      }
    }
    named("jvmMain")
    named("jvmTest")
    //==================================================
    /**
     * 等效于这些方法：
     * val commonMain by getting
     * val commonTest by getting
     * val jvmMain by getting
     */
  }
}

publishing {
  publications.withType<MavenPublication>().getByName("kotlinMultiplatform") {
    groupId = "cn.aimdie"
    artifactId = "lib-butneed"
    pom {
      name.set("lib-butneed")
      val ds = "不重要，但是很需要的一个库。"
      description.set(ds)
      val sUrl = "https://github.com/aimdie"
      url.set(sUrl)
      licenses {
        license {
          val licenseName = "Apache License 2.0"
          val licenseUrl = "https://www.apache.org/licenses/LICENSE-2.0"
          name.set(licenseName)
          url.set(licenseUrl)
        }
      }
      
      developers {
        developer {
          id.set("aimdie")
          name.set("wgyx")
          email.set("https://github.com/aimdie")
        }
      }
    }
  }
  repositories {
    maven {
      url =uri("$rootDir/testLibDir")
    }
  }
}
